### Install docker & docker-compose

##### Docker

https://docs.docker.com/installation/

Steps: 	- install
		- Create a Docker group 

sudo groupadd docker
sudo usermod -aG docker {user}
export DOCKER_HOST=tcp://localhost:4243


##### Docker-compose

https://docs.docker.com/compose/#installation-and-set-up


### Docker

##### Magic line remove and re-build images

	docker-compose stop && docker-compose rm -f && docker-compose build && sudo docker-compose up -d

##### SSH in running container

    docker exec -i -t container_name bash

##### Remove all images
	docker rmi $(docker images -q)